/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the "License").
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://opensource.org/licenses/CDDL-1.0.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 *
 * Copyright 2014 Saso Kiselkov.
 */

#include <assert.h>
#include <getopt.h>
#include <errno.h>
#include <fcntl.h>
#include <pthread.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "lz4.h"

/*
 * Straightforward implementation of a multi-threaded LZ4 (de)compressor.
 *
 * The code below implements a simple multi-threaded compressor front-end
 * to the LZ4 library.
 *
 * File Format
 * The compressed file format consists of the following structure:
 *
 * struct lz4_file {
 *	uint32_t magic = BE{0x4C, 0x5A, 0x34, 0x01} or
 *	    LE{0x01, 0x34, 0x5A, 0x4C};
 *	struct {
 *		uint32_t bufsiz;		// endianness depends on magic
 *		uint32_t cksum;			// fletcher32 (endianness
 *						// depends on magic)
 *		uint8_t lz4_stream[bufsiz];
 *	} record[until_eof];
 * };
 */

#ifdef SUNOS
#include <sys/byteorder.h>
#else /* !SUNOS */
#include <arpa/inet.h>
typedef enum {
	B_FALSE = 0,
	B_TRUE = 1
} boolean_t;
#endif /* !SUNOS */

#ifndef SUNOS
static struct option longopts[] = {
	{ "stdout",	no_argument,		NULL,	'c' },
	{ "to-stdout",	no_argument,		NULL,	'c' },
	{ "verbose",	no_argument,		NULL,	'v' },
	{ "decompress",	no_argument,		NULL,	'd' },
	{ "uncompress",	no_argument,		NULL,	'd' },
	{ "keep",	no_argument,		NULL,	'k' },
	{ "threads",	required_argument,	NULL,	't' },
	{ NULL,		0,			NULL,	0 }
};
#endif

/*
 * This is the default uncompressed input record size that we try to use.
 * We want this number to be big enough so that compression can be efficient,
 * but not too big, since these blocks get buffered on worker queues and can
 * consume quite a lot of memory. 1 MB seems a good compromise between speed
 * and efficiency, and memory requirements.
 */
#define	BLOCKSIZE	(1024 * 1024)

static boolean_t use_stdin = B_FALSE;
static int verbose = 0;
static boolean_t force = B_FALSE;
static boolean_t do_delete = B_TRUE;

/*
 * The queue depth multiplier tells us how deep at most our work queues can
 * be. Using deep queues makes sure that even if a worker is taking a lot of
 * time processing a certain buffer, the other buffers don't get blocked and
 * can carry on doing other work. We create our worker and writer queues so
 * that they are n_threads deep times this multiplier. Lowering this will
 * help keep compressor memory usage down, but might impact concurrency
 * performance (since threads may need to block if the queue fills up due to
 * one thread taking a lot of time to process a particular block).
 */
static int queue_depth_mul = 3;

extern int getopt(int, char * const[], const char *);

typedef struct dbuf {
	void *data;
	uint64_t size;
	uint64_t seq;
	struct dbuf *next, *prev;
} dbuf_t;

/*
 * A buffer queue is an object which holds a limited number of data buffers
 * in a sorted way. Each inserted buffer has a sequence number telling the
 * queue where to 'slot it in', so that the queue contents are always sorted
 * in ascending sequence order:
 *
 *      new buf:
 *      |BUF_3|
 *          \  goes here..
 *           +------------+
 *                         \
 *  list_head               | list_tail
 *    |                     |     |
 *    V                     V     V
 * |BUF_0|-|BUF_1|-|BUF_2|-...-|BUF_4|
 *
 * When buffers are taken out of the queue, they are always popped from its
 * head. In order to allow for efficient multi-threaded processing of buffers,
 * each of which can take a variable amount of time to complete, the queue
 * incorporates mechanisms for effectively dealing with out-of-order buffers.
 * Buffers are always inserted into the correct sequence based on their
 * sequence number. In addition, the list keeps track of the "next" sequence
 * number.
 *
 * When a queue is created, it starts out with a sequence number of 0. When
 * a caller tries to pop a buffer from it, they are blocked until a buffer
 * with sequence number 0 is available - only then the buffer is returned.
 * Meanwhile new queue insertions can happen further "down" the list, allowing
 * for other multi-threaded processing to take place while maintaining correct
 * data ordering. Once a buffer is popped from the queue, the sequence counter
 * of the queue is incremented, so that on the next pop it returns the next
 * buffer in sequence (or blocks until *it* becomes available).
 *
 * To make sure that the queue doesn't grow beyond a certain limit it maintains
 * a maximum distance value (max_items) from the current sequence number. If
 * the sequence number of the buffer the caller wants to insert is too large
 * (greater than current_seq + max_items), the caller is suspended until the
 * sequence counter on the queue advances enough so that the buffer's sequence
 * number fits this constraint (i.e. the list shrinks enough to accommodate it).
 */
typedef struct buf_queue {
	/* protected by mtx */
	dbuf_t *first, *last;
	uint64_t max_items;
	uint64_t seq;

	pthread_mutex_t mtx;
	pthread_cond_t in_seq, seq_advanced;
} bufq_t;

/*
 * This is the main compressor/decompressor processing state that is
 * regenerated for each input file.
 */
typedef struct {
	/* thread safe */
	bufq_t *worker_queue, *writer_queue;

	/* read only */
	boolean_t compress;

	/* only accessed from input thread */
	uint64_t next_seq;
	int n_workers;
	pthread_t *workers;
	pthread_t writer;
	uint64_t bytes_in;

	/* only accessed from writer thread */
	int out_fd;
	uint64_t bytes_out;
} proc_state_t;

/*
 * Returns a byte-swapped representation of 'x'.
 */
static inline uint16_t
bswap16(uint16_t x)
{
	return (((x & 0xff00) >> 8) | ((x & 0x00ff) << 8));
}

/*
 * Returns a byte-swapped representation of 'x'.
 */
static inline uint32_t
bswap32(uint32_t x)
{
	return (((x & 0xff000000) >> 24) | ((x & 0x00ff0000) >> 8) |
	    ((x & 0x0000ff00) << 8) | ((x & 0x000000ff) << 24));
}

/*
 * Simple Fletcher-32 implementation with support for unaligned data.
 */
static uint32_t
fletcher32(const uint16_t *data, size_t words)
{
	uint32_t sum1 = 0xffff, sum2 = 0xffff;

	while (words) {
		unsigned tlen = words > 359 ? 359 : words;
		words -= tlen;
		do {
			sum2 += sum1 += *data++;
		} while (--tlen);
		sum1 = (sum1 & 0xffff) + (sum1 >> 16);
		sum2 = (sum2 & 0xffff) + (sum2 >> 16);
	}

	/* Second reduction step to reduce sums to 16 bits */
	sum1 = (sum1 & 0xffff) + (sum1 >> 16);
	sum2 = (sum2 & 0xffff) + (sum2 >> 16);

	return (sum2 << 16 | sum1);
}

/*
 * Same as above, but byte-swapped.
 */
static uint32_t
fletcher32_bswap(const uint16_t *data, size_t words)
{
	uint32_t sum1 = 0xffff, sum2 = 0xffff;

	while (words) {
		unsigned tlen = words > 359 ? 359 : words;
		words -= tlen;
		do {
			uint16_t d = *data++;
			sum2 += sum1 += bswap16(d);
		} while (--tlen);
		sum1 = (sum1 & 0xffff) + (sum1 >> 16);
		sum2 = (sum2 & 0xffff) + (sum2 >> 16);
	}

	/* Second reduction step to reduce sums to 16 bits */
	sum1 = (sum1 & 0xffff) + (sum1 >> 16);
	sum2 = (sum2 & 0xffff) + (sum2 >> 16);

	return (bswap32((sum2 << 16) | sum1));
}

/*
 * Reads data and blocks until the entire length is read. If an error is
 * encountered this function exits the program with an error. If need_len
 * is non-zero, then we also exit with an error if the input ends before
 * len bytes have been read.
 */
static size_t
read_block(int fd, void *p, size_t len, int need_len)
{
	uint8_t *buf = p;
	size_t n;
	ssize_t s;

	assert(len > 0);
	for (n = 0; n < len; n += s) {
		switch ((s = read(fd, &buf[n], len - n))) {
		case -1:
			perror("Read error");
			exit(EXIT_FAILURE);
		case 0:
			if (need_len) {
				(void) fprintf(stderr, "Premature EOF, wanted "
				    "%lu bytes, got %lu bytes\n", len, n);
				exit(EXIT_FAILURE);
			} else {
				goto out;
			}
		}
	}
out:
	return (n);
}

/*
 * Allocates a new dbuf. After your are done with it, destroy it using
 * dbuf_destroy. By passing '0' as the size, the dbuf 'data' item will not
 * be allocated (creating an empty dbuf).
 */
static dbuf_t *
dbuf_create(uint64_t size)
{
	dbuf_t *db = calloc(sizeof (*db), 1);

	if (size != 0) {
		db->data = calloc(1, size);
		db->size = size;
	}

	return (db);
}

/*
 * Destroys and frees a dbuf previously created using dbuf_create.
 */
static void
dbuf_destroy(dbuf_t *db)
{
	free(db->data);
	free(db);
}

/*
 * Allocates a new buffer queue. The queue's initial sequence value is zero
 * and the queue can hold up to `max_items' in it.
 */
static bufq_t *
bufq_create(uint64_t max_items)
{
	bufq_t *queue = calloc(sizeof (*queue), 1);

	assert(max_items >= 1);
	(void) pthread_mutex_init(&queue->mtx, NULL);
	(void) pthread_cond_init(&queue->in_seq, NULL);
	(void) pthread_cond_init(&queue->seq_advanced, NULL);
	queue->max_items = max_items;
	return (queue);
}

/*
 * Destroys a buffer queue previously created using dbuf_create. Any buffers
 * queued up on the queue are destroyed as well.
 */
static void
bufq_destroy(bufq_t *queue)
{
	dbuf_t *db, *db_next;

	for (db = queue->first; db != NULL; db = db_next) {
		db_next = db->next;
		dbuf_destroy(db);
	}

	(void) pthread_mutex_destroy(&queue->mtx);
	(void) pthread_cond_destroy(&queue->in_seq);
	(void) pthread_cond_destroy(&queue->seq_advanced);
	free(queue);
}

/*
 * Puts a buffer into the queue. If the buffer being inserted has a sequence
 * number between the queue's current sequence counter value and seq+max_items,
 * the insert proceeds immediately. If the value is larger than seq+max_items,
 * the caller is blocked until the queue advances sufficiently to incorporate
 * the new buffer in the max_items requirement. If the buffer's sequence value
 * is lower than the queue's sequence number, or is the same as that of a
 * buffer already on the queue, an assertion is tripped.
 */
static void
bufq_put(dbuf_t *db, bufq_t *queue)
{
	dbuf_t *elem;

	(void) pthread_mutex_lock(&queue->mtx);

	/* queue is filling up, block the caller */
	while (db->seq > queue->seq + queue->max_items)
		(void) pthread_cond_wait(&queue->seq_advanced, &queue->mtx);
	assert(db->seq >= queue->seq);

	if (queue->last != NULL && queue->last->seq < db->seq) {
		/* shortcut for tail-insert */
		queue->last->next = db;
		db->prev = queue->last;
		queue->last = db;
		/*
		 * no need to send an in-seq broadcast here, since we're
		 * changing the queue tail
		 */
	} else {
		/* the buffer is out of order, find where it belongs */
		for (elem = queue->first; elem != NULL; elem = elem->next) {
			assert(elem->seq != db->seq);
			if (db->seq < elem->seq) {
				db->next = elem;
				db->prev = elem->prev;
				if (elem->prev)
					elem->prev->next = db;
				elem->prev = db;
				if (queue->first == elem)
					queue->first = db;
				break;
			}
		}

		if (queue->first == NULL) {
			/* this is the first entry */
			queue->first = queue->last = db;
		} else if (elem == NULL) {
			/* this is the last entry */
			assert(queue->last != NULL);
			queue->last->next = db;
			db->prev = queue->last;
			queue->last = db;
		}

		/*
		 * Unblock any threads waiting for an in-sequence event.
		 * N.B. this can only occur on an insert-before, not on the
		 * tail-insert shortcut case above.
		 */
		if (queue->first->seq == queue->seq)
			(void) pthread_cond_broadcast(&queue->in_seq);
	}

	(void) pthread_mutex_unlock(&queue->mtx);
}

/*
 * Pops the next in-sequence buffer from the queue. If the next buffer is
 * not yet on the queue, the called thread is blocked until it is added.
 */
static dbuf_t *
bufq_getnext(bufq_t *queue)
{
	dbuf_t *db;

	(void) pthread_mutex_lock(&queue->mtx);

	while (queue->first == NULL || queue->seq < queue->first->seq) {
		/*
		 * queue empty or out of sequence, block waiting for an
		 * in-sequence event
		 */
		(void) pthread_cond_wait(&queue->in_seq, &queue->mtx);
	}
	assert(queue->first != NULL);
	assert(queue->first->seq == queue->seq);

	/* dequeue the first element */
	db = queue->first;
	queue->first = db->next;
	if (queue->last == db)
		queue->last = NULL;
	if (db->next != NULL)
		db->next->prev = NULL;
	db->next = db->prev = NULL;

	/* advance the sequence counter */
	queue->seq++;
	(void) pthread_cond_broadcast(&queue->seq_advanced);

	(void) pthread_mutex_unlock(&queue->mtx);

	return (db);
}

/*
 * Compresses a data buffer. After compression, the data buffer's 'data'
 * item is substituted with the compressed version. The uncompressed version
 * is free()d.
 */
static void
compress_buf(dbuf_t *db)
{
	void *in_buf, *out_buf;
	uint32_t in_len, out_len;
	uint32_t cksum = 0;

	in_buf = db->data;
	in_len = db->size;
	out_buf = malloc(LZ4_compressBound(in_len) + 8);
	out_len = LZ4_compress(in_buf, (char *) out_buf + 8, in_len,
	    LZ4_compressBound(in_len));

	/* prepend a compression segment header (32-bit bufsiz & cksum) */
	*(uint32_t *)out_buf = out_len;
	cksum = fletcher32((uint16_t *) out_buf + 4, out_len / 2);
	(void) memcpy((uint8_t *) out_buf + 4, &cksum, sizeof (cksum));

	db->data = out_buf;
	db->size = out_len + 8;
	free(in_buf);
}

/*
 * Decompresses a data buffer. The segment header (bufsiz & cksum) must have
 * already been stripped away. This function expects that 'db' only holds the
 * actual compressed data of the correct length (in db->size).
 */
static void
decompress_buf(dbuf_t *db)
{
	void *in_buf, *out_buf;
	int in_len, out_len;

	in_buf = db->data;
	in_len = db->size;
	out_buf = malloc(BLOCKSIZE);
	out_len = LZ4_uncompress_unknownOutputSize(in_buf, out_buf, in_len,
	    BLOCKSIZE);
	if (out_len < 0) {
		(void) fprintf(stderr, "Error decoding buffer, LZ4 stream is "
		    "damaged\n");
		exit(EXIT_FAILURE);
	}

	db->data = out_buf;
	db->size = out_len;
	free(in_buf);
}

/*
 * This is a compression worker thread.
 * The worker simply loops around waiting for buffers on the worker_queue to
 * process (either compress or decompress). When an empty buffer is received
 * (a buffer with db->data == NULL), the worker terminates.
 */
static void
worker(proc_state_t *ps)
{
	for (;;) {
		dbuf_t *db = bufq_getnext(ps->worker_queue);

		if (db->data != NULL) {
			/*
			 * non-empty buffers are compressed/decompressed
			 * and then enqueued on the writer_queue
			 */
			if (ps->compress)
				compress_buf(db);
			else
				decompress_buf(db);
			bufq_put(db, ps->writer_queue);
		} else {
			/* empty buffers cause us to terminate */
			dbuf_destroy(db);
			break;
		}
	}
}

/*
 * This is a writer thread.
 * The writer thread writes out the compression/decompression results. If the
 * operation to be performed is compression, the writer prepends an LZ4 file
 * header. Aftewards it just waits for buffers on the writer_queue and writes
 * them out in sequence. When an empty input buffer is received, the writer
 * thread terminates.
 */
static void
writer(proc_state_t *ps)
{
	/* write a file header when doing compression */
	if (ps->compress) {
		const uint32_t magic = 0x4C5A3401;

		(void) write(ps->out_fd, &magic, sizeof (magic));
	}

	/* loop around waiting for input buffers */
	for (;;) {
		dbuf_t *db = bufq_getnext(ps->writer_queue);

		if (db->data != NULL) {
			/* non-empty buffers are written */
			int n = write(ps->out_fd, db->data, db->size);

			if (n == -1) {
				perror("Error writing data");
				exit(EXIT_FAILURE);
			} else if (n == (int) db->size) {
				/* write ok */
				dbuf_destroy(db);
				ps->bytes_out += n;
			} else {
				(void) fprintf(stderr, "Error writing data: "
				    "short byte count written");
				exit(EXIT_FAILURE);
			}
		} else {
			/* empty buffers cause us to terminate */
			dbuf_destroy(db);
			break;
		}
	}
}

/*
 * Alloc's and initializes a full processing state for a single input->output
 * combo. This function initializes all worker and writer threads and returns
 * a processing state to which the caller can then enqueue buffers (on the
 * worker_queue). These buffers will then be written to 'out_fd'.
 */
static proc_state_t *
proc_state_create(boolean_t compress, int n_workers, int out_fd)
{
	int i, res;
	proc_state_t *ps = calloc(sizeof (*ps), 1);

	ps->worker_queue = bufq_create(n_workers * queue_depth_mul);
	ps->writer_queue = bufq_create(n_workers * queue_depth_mul);
	ps->out_fd = out_fd;
	ps->compress = compress;
	ps->n_workers = n_workers;
	ps->workers = malloc(sizeof (*ps->workers) * n_workers);

	/* spawn all workers */
	for (i = 0; i < n_workers; i++) {
		if ((res = pthread_create(&ps->workers[i], NULL,
		    (void *(*)(void *)) worker, ps)) != 0) {
			(void) fprintf(stderr, "Error creating worker thread: "
			    "%s\n", strerror(res));
			exit(EXIT_FAILURE);
		}
	}

	/* spawn writer */
	if ((res = pthread_create(&ps->writer, NULL, (void *(*)(void *)) writer,
	    ps)) != 0) {
		(void) fprintf(stderr, "Error creating writer thread: %s\n",
		    strerror(res));
		exit(EXIT_FAILURE);
	}

	return (ps);
}

/*
 * Destroys a processing state previously created using proc_state_create.
 * This must be called from the main input thread because it expects that no
 * more input is placed on the worker queues.
 * This function signals all worker threads to finish up (work that is 'in
 * flight' will be finished), terminate and then for all output writing to do
 * so as well. After this function returns, the processing state is invalid
 * and cannot be used anymore.
 */
static void
proc_state_destroy(proc_state_t *ps, uint64_t *bytes_out)
{
	dbuf_t *db;
	int i;

	/*
	 * First shut down all workers by queueing empty buffers. These buffers
	 * are not forwarded to the writer queue, so the sequence counters for
	 * the two queues will get out of sync. That's OK, since we're not
	 * going to be using them for anything anymore.
	 */
	for (i = 0; i < ps->n_workers; i++) {
		db = dbuf_create(0);
		db->seq = ps->next_seq + i;
		bufq_put(db, ps->worker_queue);
	}
	for (i = 0; i < ps->n_workers; i++) {
		(void) pthread_join(ps->workers[i], NULL);
	}

	/* Grab total output statistics */
	*bytes_out = ps->bytes_out;

	/*
	 * Now that all workers have shut down, append a single empty
	 * buffer to the writer queue. Again, watch the sequence numbers.
	 */
	db = dbuf_create(0);
	db->seq = ps->next_seq;
	bufq_put(db, ps->writer_queue);
	(void) pthread_join(ps->writer, NULL);

	/* and free all allocated memory */
	free(ps->workers);
	bufq_destroy(ps->worker_queue);
	bufq_destroy(ps->writer_queue);
	free(ps);
}

/*
 * Reads the next input block. Depending on whether we're compressing or
 * decompressing this will either read our maximum input block size or decode
 * the start block marker and then read in the compressed buffer.
 */
static dbuf_t *
read_next_block(boolean_t compress, int in_fd, boolean_t bswap)
{
	dbuf_t *db;
	int n;

	if (compress) {
		db = dbuf_create(BLOCKSIZE);
		switch ((n = read_block(in_fd, db->data, BLOCKSIZE, 0))) {
		case 0:
			dbuf_destroy(db);
			return (NULL);
		default:
			/* input data block read */
			db->size = n;
			break;
		}
	} else {
		uint32_t cksum_read, cksum_computed;
		uint32_t bufsiz;
		int n;

		db = dbuf_create(LZ4_compressBound(BLOCKSIZE) + 8);
		/*
		 * Read the block start marker containing the big-endian
		 * encoded buffer length.
		 */
		switch ((n = read_block(in_fd, &bufsiz, sizeof (bufsiz), 0))) {
		case 0:
			dbuf_destroy(db);
			return (NULL);
		case 4:
			break;
		default:
			fprintf(stderr, "Premature EOF: input truncated\n");
			exit(EXIT_FAILURE);
		}

		if (bswap)
			bufsiz = bswap32(bufsiz);
		if ((int)bufsiz > LZ4_compressBound(BLOCKSIZE)) {
			(void) fprintf(stderr,
			    "Invalid compressed buffer size\n");
			exit(EXIT_FAILURE);
		}

		/* Read the checksum */
		(void) read_block(in_fd, &cksum_read, sizeof (cksum_read), 1);

		/* Now read in the data block */
		read_block(in_fd, db->data, bufsiz, 1);
		db->size = bufsiz;

		/* Validate the block against the checksum we read */
		if (!bswap)
			cksum_computed = fletcher32(db->data, bufsiz / 2);
		else
			cksum_computed = fletcher32_bswap(db->data, bufsiz / 2);
		if (cksum_read != cksum_computed) {
			(void) fprintf(stderr, "Bad input: checksum mismatch, "
			    "file appears to be corrupt.\n");
			exit(EXIT_FAILURE);
		}
	}

	return (db);
}

/*
 * Reads and checks an LZ4 file header from 'fd'. If the header is byte-swapped
 * relative to us (meaning the file was created on a machine with opposite
 * endianness), the bswap pointer is filled with B_TRUE, otherwise it is filled
 * with B_FALSE.
 */
static void
check_lz4_file_header(int fd, boolean_t *bswap)
{
	uint32_t hdr;

	(void) read_block(fd, &hdr, sizeof (hdr), 1);

	switch (hdr) {
	case 0x4C5A3401:
		/* magic ok & byte order ok */
		*bswap = B_FALSE;
		break;
	case 0x01345A4C:
		/* magic ok & byte order swapped */
		*bswap = B_TRUE;
		break;
	default:
		(void) fprintf(stderr, "Bad header magic or file version "
		    "mismatch.\n");
		exit(EXIT_FAILURE);
	}
}

/*
 * Processes a single input file descriptor for compression or decompression.
 * This is the main input reader function which prepares all processing state
 * and then reads all input data and queues it to our workers for compression
 * or decompression. Afterwards the processing state is cleaned up and when
 * this function returns, the input file has been completely processed into
 * the output file.
 */
static void
proc_fd(boolean_t compress, int n_threads, int in_fd, int out_fd,
    uint64_t *bytes_in, uint64_t *bytes_out)
{
	dbuf_t *db;
	boolean_t bswap = B_FALSE;
	proc_state_t *ps;

	if (!compress)
		check_lz4_file_header(in_fd, &bswap);

	ps = proc_state_create(compress, n_threads, out_fd);
	while ((db = read_next_block(compress, in_fd, bswap)) != NULL) {
		db->seq = ps->next_seq++;
		ps->bytes_in += db->size;
		bufq_put(db, ps->worker_queue);
	}
	*bytes_in = ps->bytes_in;
	proc_state_destroy(ps, bytes_out);
}

/*
 * Prints the verbose message at the start of processing a file.
 */
static void
start_proc_file(const char *filename)
{
	if (verbose) {
		(void) fprintf(stderr, "%s: ", filename);
		(void) fflush(stderr);
	}
}

/*
 * Prints the verbose message at the end of processing a file.
 */
static void
finish_proc_file(uint64_t bytes_in, uint64_t bytes_out)
{
	if (verbose >= 2)
		(void) fprintf(stderr, "%llu bytes in, %llu bytes out, "
		    "%.01f%%\n", (long long unsigned) bytes_in,
		    (long long unsigned) bytes_out,
		    100 - ((double) bytes_out / bytes_in) * 100);
	else if (verbose >= 1)
		(void) fprintf(stderr, "%.01f%%\n",
		    100 - ((double) bytes_out / bytes_in) * 100);
}

/*
 * Processes a single file by name. This does the file name manipulation,
 * opens the input and output files and passes those to proc_fd.
 */
static void
proc_filename(boolean_t do_compress, int n_threads, const char *in_file)
{
	char *out_file;
	int out_file_len;
	struct stat st;
	int in_fd, out_fd;
	uint64_t bytes_in, bytes_out;

	/*
	 * Determine the output file name. For compression we add a .lz4
	 * suffix. For decompression we check that the .lz4 suffix is present
	 * and if it is, we strip it.
	 */
	if (do_compress) {
		if (strlen(in_file) > 4 &&
		    strcmp(in_file + strlen(in_file) - 4, ".lz4") == 0) {
			(void) fprintf(stderr, "Input file %s already "
			    "compressed (.lz4 suffix present)\n", in_file);
			exit(2);
		}
		out_file_len = strlen(in_file) + 4 + 1;
		out_file = malloc(out_file_len);
		(void) snprintf(out_file, out_file_len, "%s.lz4", in_file);
	} else {
		if (strlen(in_file) < 4 ||
		    strcmp(in_file + strlen(in_file) - 4, ".lz4")) {
			(void) fprintf(stderr, "Input file %s not compressed "
			    "(missing .lz4 suffix).\n", in_file);
			exit(2);
		}
		out_file_len = strlen(in_file) + 1 - 4;
		out_file = malloc(out_file_len);
		(void) memcpy(out_file, in_file, out_file_len - 1);
		out_file[out_file_len - 1] = 0;
	}
	if (stat(in_file, &st) == -1) {
		(void) fprintf(stderr, "Cannot stat input file %s: %s\n",
		    in_file, strerror(errno));
		exit(EXIT_FAILURE);
	}
	if (st.st_size == 0) {
		(void) fprintf(stderr, "Cannot process input file %s: file "
		    "empty\n", in_file);
		exit(EXIT_FAILURE);
	}

	/*
	 * Check if the output exists. We may need to prompt the user.
	 */
	if (stat(out_file, &st) == 0 && force == B_FALSE) {
		char response[3] = {0, 0, 0};

		(void) printf("Warning: destination file %s already exists.\n"
		    "Overwrite? [y/n]: ", out_file);
		(void) fflush(stdout);
		if (scanf("%2s", response) != 1 || response[0] != 'y' ||
		    response[1] != '\0') {
			(void) fprintf(stderr, "Not confirmed, bailing out.\n");
			exit(2);
		}
	}

	in_fd = open(in_file, O_RDONLY);
	if (in_fd == -1) {
		(void) fprintf(stderr, "Cannot open file %s for reading: %s\n",
		    in_file, strerror(errno));
		exit(EXIT_FAILURE);
	}

	out_fd = open(out_file, O_WRONLY | O_TRUNC | O_CREAT, 0644);
	if (out_fd == -1) {
		(void) fprintf(stderr, "Cannot open file %s for writing: %s\n",
		    out_file, strerror(errno));
		exit(EXIT_FAILURE);
	}

	start_proc_file(in_file);
	proc_fd(do_compress, n_threads, in_fd, out_fd, &bytes_in, &bytes_out);
	finish_proc_file(bytes_in, bytes_out);

	(void) close(out_fd);
	(void) close(in_fd);
	free(out_file);

	if (do_delete) {
		if (unlink(in_file) == -1) {
			(void) fprintf(stderr, "Cannot delete input file %s: "
			    "%s\n", in_file, strerror(errno));
			exit(EXIT_FAILURE);
		}
	}
}

/*
 * Determines whether the user called us as a compressor or decompressor.
 * This is simply done by looking for the word "lz4uncompress" at the end
 * of our command name in argv0.
 */
static boolean_t
want_decompress(const char *argv0)
{
	return (strstr(argv0, "lz4uncompress") ==
	    argv0 + strlen(argv0) - strlen("lz4uncompress"));
}

static void
print_help(const char *argv0)
{
	(void) printf("Usage: %s [-h?cvfdk] [-t <threads>] [<file> ...]\n"
	    "Options:\n"
	    " -h | -?: Print this help screen and exit.\n"
	    " -c: With a single file name on the command line, write output "
	    "to stdout. Also forces operation on stdin/stdout even if they "
	    "appear to be a tty.\n"
	    " -v: Be more verbose about what we're doing. Multiple -v options "
	    "increase\n"
	    "     verbosity.\n"
	    " -f: Force operation, don't prompt before overwriting files.\n"
	    " -d: Force decompression regardless of invocation name.\n"
	    " -k: Keep (don't delete) the input file after operation "
	    "completes.\n"
	    " -t <threads>: Number of processing threads (min: 1).\n"
	    " -q <queue_depth_mul>: Multiplier for the queue depth. You can "
	    "use this\n"
	    "    to limit memory usage to some extent.\n",
	    argv0);
}

int
main(int argc, char **argv)
{
	int opt;
	boolean_t do_compress = !want_decompress(argv[0]);
	int n_threads = sysconf(_SC_NPROCESSORS_ONLN);

#ifdef SUNOS
	while ((opt = getopt(argc, argv, "h(help)?d(decompress)(uncompress)"
	    "k(keep)c(stdout)(to-stdout)v(verbose)f(force)t:(threads)"
	    "q:(queue-depth)")) != -1) {
#else
	while ((opt = getopt_long(argc, argv, "h?dkcvft:q:", longopts, NULL))
	    != -1) {
#endif
		switch (opt) {
		case 'h':
		case '?':
			print_help(argv[0]);
			return (0);
		case 'd':
			do_compress = B_FALSE;
			break;
		case 'k':
			do_delete = B_FALSE;
			break;
		case 'c':
			use_stdin = B_TRUE;
			break;
		case 'v':
			verbose++;
			break;
		case 'f':
			force = B_TRUE;
			break;
		case 't':
			n_threads = atoi(optarg);
			if (n_threads < 1) {
				(void) fprintf(stderr, "Invalid thread count. "
				    "Try %s -h for help.\n", argv[0]);
				return (2);
			}
			break;
		case 'q':
			queue_depth_mul = atoi(optarg);
			if (queue_depth_mul < 1) {
				(void) fprintf(stderr, "Invalid queue depth. "
				    "Try %s -h for help.\n", argv[0]);
				return (2);
			}
		default:
			(void) fprintf(stderr, "Try %s -h for help.\n",
			    argv[0]);
			return (2);
		}
	}

	if (optind == argc) {
		if ((!isatty(STDIN_FILENO) && !isatty(STDOUT_FILENO)) ||
		    use_stdin) {
			uint64_t bytes_in, bytes_out;

			start_proc_file("(stdin)");
			proc_fd(do_compress, n_threads, STDIN_FILENO,
			    STDOUT_FILENO, &bytes_in, &bytes_out);
			finish_proc_file(bytes_in, bytes_out);
		} else {
			(void) fprintf(stderr, "Missing file name argument and "
			    "input/output is a tty.\nIf this is what you "
			    "really want, then use the -c option.\n");
			return (2);
		}
	} else {
		int i;

		if (use_stdin) {
			int fd;
			uint64_t bytes_in, bytes_out;

			if (optind + 1 < argc) {
				(void) fprintf(stderr, "When using the -c "
				    "option, only specify at most one input "
				    "file, since %s doesn't support multi-file "
				    "operation in a single stream\n.", argv[0]);
				return (2);
			}

			fd = open(argv[optind], O_RDONLY);
			if (fd == -1) {
				perror("Cannot open input file");
				exit(EXIT_FAILURE);
			}
			start_proc_file(argv[optind]);
			proc_fd(do_compress, n_threads, fd, STDOUT_FILENO,
			    &bytes_in, &bytes_out);
			finish_proc_file(bytes_in, bytes_out);
			close (fd);
		} else {
			for (i = optind; i < argc; i++)
				proc_filename(do_compress, n_threads, argv[i]);
		}
	}

	return (0);
}
