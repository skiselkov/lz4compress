# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the "License").
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
# or http://opensource.org/licenses/CDDL-1.0.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/OPENSOLARIS.LICENSE.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets "[]" replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#
# Copyright 2014 Saso Kiselkov.

ifeq ($(bits),)
  bits = 64
endif
ifeq ($(prefix),)
  prefix=/usr/local
endif
ifeq ($(gcc),)
  gcc = gcc
endif

OS = $(shell uname | tr '[:lower:]' '[:upper:]')
CFLAGS = -D_LARGEFILE64_SOURCE -m$(bits) -W -Wall -D$(OS)
LDFLAGS = -m$(bits) -lpthread -W -Wall

ifeq ($(OS),SUNOS)
  INSTALL = ginstall
else
  INSTALL = install
endif

ifeq ($(debug),yes)
  CFLAGS += -O0 -ggdb3
else
  CFLAGS += -O3 -fomit-frame-pointer
endif

OBJS := lz4compress.o lz4.o

all : lz4compress lz4uncompress

lz4compress : $(OBJS)
	$(gcc) $(LDFLAGS) -o $@ $(OBJS)

lz4uncompress : lz4compress
	ln -f lz4compress lz4uncompress

%.o : %.c
	$(gcc) $(CFLAGS) -c -o $@ $^

install : lz4compress
	$(INSTALL) -m 755 lz4compress $(prefix)/bin/lz4compress
	strip $(prefix)/bin/lz4compress
	ln -f $(prefix)/bin/lz4compress $(prefix)/bin/lz4uncompress
	gzip -c < lz4compress.1 > lz4compress.1.gz
	$(INSTALL) -m 644 lz4compress.1.gz \
	    $(prefix)/share/man/man1/lz4compress.1.gz
	rm lz4compress.1.gz
	ln -sf lz4compress.1.gz $(prefix)/share/man/man1/lz4uncompress.1.gz

uninstall :
	rm -f $(prefix)/bin/lz4compress $(prefix)/bin/lz4uncompress \
	    $(prefix)/share/man/man1/lz4compress.1.gz \
	    $(prefix)/share/man/man1/lz4uncompress.1.gz

clean :
	rm -f lz4compress lz4uncompress $(OBJS)
