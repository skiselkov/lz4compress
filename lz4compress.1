'\" t
.\"
.\" CDDL HEADER START
.\"
.\" The contents of this file are subject to the terms of the
.\" Common Development and Distribution License (the "License").
.\" You may not use this file except in compliance with the License.
.\"
.\" You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
.\" or http://opensource.org/licenses/CDDL-1.0.
.\" See the License for the specific language governing permissions
.\" and limitations under the License.
.\"
.\" When distributing Covered Code, include this CDDL HEADER in each
.\" file and include the License file at usr/src/OPENSOLARIS.LICENSE.
.\" If applicable, add the following below this CDDL HEADER, with the
.\" fields enclosed by brackets "[]" replaced with your own identifying
.\" information: Portions Copyright [yyyy] [name of copyright owner]
.\"
.\" CDDL HEADER END
.\"
.\"
.\" Copyright 2014 Saso Kiselkov.
.\"
.TH LZ4COMPRESS 1 "Sep 10, 2014"
.SH NAME
lz4compress, lz4uncompress \- compress and expand files
.SH SYNOPSIS
.LP
.nf
\fBlz4compress\fR [\fB-h?cvfdk\fR] [\fB-t <threads>\fR] [\fB<file> ...\fR]
.fi

.LP
.nf
\fBlz4uncompress\fR [\fB-h?cvfdk\fR] [\fB-t <threads>\fR] [\fB<file> ...\fR]

.SH DESCRIPTION
.LP
The \fBlz4compress\fR and \fBlz4uncompress\fR commands are a
high-performance multi-threaded data compressor and decompressor based
on the LZ4 algorithm. The options are purposely similar to those for
\fBgzip\fR(1) to facilitate simple interchange in scripts. The reason for
using \fBlz4compress\fR over \fBgzip\fR is because LZ4 is a very fast
real-time compressor that trades some of the achieved compression ratio
for much faster compression and decompression performance. On typical
datasets where Gzip achieves around 67% compression, LZ4 only gets
around 51% but is over 10x faster at both compression and decompression
and much faster at compression of incompressible data. This is useful in
applications where the cost of extra CPU cycles is much higher than the
cost of some saved data bandwidth (e.g. when doing asynchronous
replication of ZFS snapshots, where the generated data stream is only
ever read once at the receiving machine).

.SH OPTIONS
.sp
.ne 2
.na
\fB\fB-h\fR\fR | \fB\fB--help\fR\fR | \fB\fB-?\fR\fR
.ad
.RS 6n
Shows a short help screen and exits.
.RE

.sp
.ne 2
.na
\fB\fB-c\fR\fR | \fB\fB--stdout\fR\fR | | \fB\fB--to-stdout\fR\fR
.ad
.RS 6n
When a single file name is specified together with the \fB-c\fR option,
the command assumes that the user wants to compress/decompress the file,
but instead of replacing it with a processed version, output is written
to standard output. The option can also be used to force operation on
stdin/stdout even if the command thinks they are a tty. When no input
file names are specified, the command automatically assumes the user
wants to process standard input to standard output, so adding \fB-c\fR
is not necessary (unless either of them is a tty).
.RE

.sp
.ne 2
.na
\fB\fB-v\fR\fR | \fB\fB--verbose\fR\fR
.ad
.RS 6n
Be verbose. This prints compression/decompression statistics to stderr
when a file is done processing. Multiple \fB-v\fR options increase
verbosity.
.RE

.sp
.ne 2
.na
\fB\fB-f\fR\fR | \fB\fB--force\fR\fR
.ad
.RS 6n
If the output file(s) already exists, don't prompt and overwrite.
.RE

.sp
.ne 2
.na
\fB\fB-d\fR\fR | \fB\fB--decompress\fR\fR | \fB\fB--uncompress\fR\fR
.ad
.RS 6n
Force decompression. This is automatically done when the command is
invoked under the \fBlz4uncompress\fR name.
.RE

.sp
.ne 2
.na
\fB\fB-k\fR\fR | \fB\fB--keep\fR\fR
.ad
.RS 6n
Keep the input file(s) and don't delete it after processing has finished.
.RE

.sp
.ne 2
.na
[\fB\fB-t\fR\fR|\fB\fB--threads\fR\fR] <\fB\fBthreads\fR\fR>
.ad
.RS 6n
Number of compressor/decompressor threads to spawn when processing. By
default this will automatically be set to the number of available CPUs
on your machine to provide maximum performance, but you can use this
option to set it anything else (minimum: 1). Input and output processing
is done in separate threads, so even when set to the minimum value, the
command will still appear in \fBps\fR(1) as running 3 threads, but only
one of them will be doing the CPU-intensive compression/decompression
work. When compressing, block checksums are computed in the compression
worker threads, but when decompressing, they are verified by the input
thread prior to handoff to a decompressor thread.
.RE

.SH EXAMPLES
.LP
\fBExample 1\fR Compressing files
.sp
The following command compresses two files \fBfoo\fR and \fBbar\fR and
replaces them with their compressed versions \fBfoo.lz4\fR and
\fBbar.lz4\fR.
.sp
.in +2
.nf
# \fBlz4compress foo bar\fR
.fi
.in -2

.LP
\fBExample 2\fR Decompressing files
.sp
The following command reverses the operation applied in \fBExample 1\fR.
.sp
.in +2
.nf
# \fBlz4uncompress foo.lz4 bar.lz4\fR
.fi
.in -2

.LP
\fBExample 3\fR Compressing a ZFS send stream and transferring it over the wire
.sp
The following sequence of commands creates a ZFS send stream from a ZFS
snapshot, compresses it, transfers it over SSH, decompresses it at the
receiving machine and receives it into a dataset.
.sp
.in +2
.nf
# \fBzfs send foo@bar | lz4compress | \\
  ssh remote_box 'lz4uncompress | zfs receive baz'\fR
.fi
.in -2

.LP
\fBExample 4\fR Compressing a file and transferring it over the wire
.sp
The following sequence of commands compresses a single file from disk and
immediately transfers it to a receiving machine.
.sp
.in +2
.nf
# \fBlz4compress -c foo | ssh remote_box 'cat > foo.lz4'\fR
.fi
.in -2

.SH EXIT STATUS
.LP
The following exit values are returned:
.sp
.ne 2
.na
\fB\fB0\fR\fR
.ad
.sp .6
.RS 4n
Successful completion.
.RE

.sp
.ne 2
.na
\fB\fB1\fR\fR
.ad
.sp .6
.RS 4n
An error occurred.
.RE

.sp
.ne 2
.na
\fB\fB2\fR\fR
.ad
.sp .6
.RS 4n
Invalid command line options were specified.
.RE

.SH SEE ALSO
.LP
\fBgzip\fR(1), \fBssh\fR(1)
.sp
.LP
For information about the \fBLZ4\fR algorithm go to
\fIhttps://code.google.com/p/lz4/\fR.
